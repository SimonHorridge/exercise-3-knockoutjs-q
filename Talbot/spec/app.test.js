﻿/// <reference path="c:\Users\Simon\documents\visual studio 2013\Projects\Talbot\Talbot\src/app.js" />
/// <reference path="c:\Users\Simon\documents\visual studio 2013\Projects\Talbot\Talbot\src/ingredient.js" />
/// <reference path="c:\Users\Simon\documents\visual studio 2013\Projects\Talbot\Talbot\src/recipe.js" />
/// <reference path="c:\Users\Simon\documents\visual studio 2013\Projects\Talbot\Talbot\lib/jasmine-2.3.4/jasmine.js" />

describe('', function () {
    var findRecipe = function (recipes, name) {
        var rtn;

        recipes.some(function (recipe) {
            if (recipe.Name === name) {
                rtn = recipe;
                return true;
            }
        })
        return rtn;
    }

    it('test ingredients', function (done) {

        var model = new window.talbot.AppViewModel();

        var keyRecipe = ("Rec" + Math.random()).replace(".", "")

        model.getRecipes().then(function () {
            var recipe = model.addRecipe()
            recipe.Name = keyRecipe
            recipe.PrepTime = "PrepTime"
            recipe.CookingTime = "CookingTime"

            recipe.addIngredient({ Name: "Ingredient 1", Quantity: "Quantity 1" })
            recipe.addIngredient({ Name: "Ingredient 2", Quantity: "Quantity 2" })
            model.saveChangesAndReload().then(function () {

                recipe = findRecipe(model.recipes(), keyRecipe)

                expect(recipe).toBeDefined();

                expect(recipe.Ingredients().length).toBe(2)

                recipe.Ingredients()[0].Quantity = "New Quantity On Existing";
                recipe.deleteIngredient(recipe.Ingredients()[0]);
                recipe.addIngredient({ "Name": "New Ingredient", Quantity: "New Quantity On New" })

                model.saveChangesAndReload().then(function () {
                    recipe = findRecipe(model.recipes(), keyRecipe)

                    expect(recipe).toBeDefined();
                    expect(recipe.Ingredients().length).toBe(2)

                    model.deleteRecipe(recipe)

                    model.saveChangesAndReload().then(function () {
                        recipe = findRecipe(model.recipes(), keyRecipe)

                        expect(recipe).not.toBeDefined();

                        done();
                    })
                })
            })
        })

    })

    it('test recipe and  ingredient classes', function () {
        var recipe = new talbot.Recipe({ Id: 1, Name: "Test Recipe", PrepTime: "preptime: 10 mins", CookingTime: "cookingTime: 1 hour" })

        expect(recipe.Name).toBe("Test Recipe");
        expect(recipe.PrepTime).toBe("preptime: 10 mins");
        expect(recipe.CookingTime).toBe("cookingTime: 1 hour");
        expect(recipe.Ingredients()).toEqual([]);

        var ingrident1 = recipe.addIngredient({ Id: 1, Name: "Milk", Quantity: "250 ml" })

        expect(ingrident1.Name).toBe("Milk");
        expect(ingrident1.Quantity).toBe("250 ml");
        expect(recipe.Ingredients().length).toBe(1);

    })

    it('test app model', function (done) {
        var model = new window.talbot.AppViewModel();
        model.getRecipes().then(function () {
            var keyRecipe = ("Rec" + Math.random()).replace(".", "")
            var recipeCount = model.recipes().length

            var recipe = model.addRecipe()
            recipe.Name = keyRecipe
            recipe.PrepTime = "New PrepTime"
            recipe.CookingTime = "New CookingTime"

            recipe.addIngredient({ Name: "Ingredient 1", Quantity: "Quantity 1" })
            recipe.addIngredient({ Name: "Ingredient 2", Quantity: "Quantity 2" })

            expect(recipe.isNew()).toBe(true)

            expect(recipe.Ingredients().length).toBe(2);

            recipe.Ingredients().map(function (ingredient) {
                expect(ingredient.isNew()).toBe(true);
            })

            expect(model.recipes().length).toBe(recipeCount + 1)

            model.saveChangesAndReload().then(function () {

                expect(model.recipes().length).toBe(recipeCount + 1)

                model.recipes().map(function (recipe) {
                    expect(recipe.hasChanges()).toBe(false)
                    expect(recipe.isNew()).toBe(false)

                    recipe.Ingredients().map(function (ingredient) {
                        expect(ingredient.hasChanges()).toBe(false);
                        expect(ingredient.isNew()).toBe(false);
                    })
                })

                recipe = findRecipe(model.recipes(), keyRecipe)
                recipe.CookingTime = "Newer Cook Time"

                expect(recipe.hasChanges()).toBe(true)

                model.saveChangesAndReload().then(function () {
                    expect(model.recipes().length).toBe(recipeCount + 1)
                    recipe = findRecipe(model.recipes(), keyRecipe)

                    expect(recipe.CookingTime).toBe("Newer Cook Time")

                    model.recipes().map(function (recipe) {
                        expect(recipe.hasChanges()).toBe(false)
                        expect(recipe.isNew()).toBe(false)
                    })

                    model.deleteRecipe(recipe)

                    model.saveChangesAndReload().then(function () {
                        recipe = findRecipe(model.recipes(), keyRecipe)
                        expect(model.recipes().length).toBe(recipeCount)
                        expect (recipe).not.toBeDefined()
                        done();
                    })
                })
            });

        });

        // Also - in a production environment, a test server/ mock ajax method would be used.
    })
})