﻿(function () {
    "use strict"
    window.talbot = window.talbot || {};

    talbot.Ingredient = function (src) {
        var self = this;
        for (var val in src) {
            this[val] = src[val];
        }

        self.orig = function () {
            return src
        }

        self.isValid = function () {
            return !!(self.Name && self.Quantity)
        }

        self.isNew = function () {
            return !self.Id
        }

        self.hasChanges = function () {
            var orig = self.orig()
            if (self.CookingTime !== orig.CookingTime) {
                return true;
            }
            if (self.PrepTime !== orig.PrepTime) {
                return true;
            }
            if (self.CookingTime !== orig.CookingTime) {
                return true;
            }

            return false
        }
    }         
}())