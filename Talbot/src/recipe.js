﻿(function () {
    "use strict"
    window.talbot = window.talbot || {};


    talbot.Recipe = function (src) {
        var self = this;
        self.deletedIngredients = [];

        if (src) {
            for (var val in src) {
                this[val] = src[val];
            }
            self.orig = function () {
                return src;
            }

        } else {
            return new talbot.Recipe({
                Name: "",
                PrepTime: "",
                CookingTime: ""
            })
        }

        self.isValid = function () {
            return (
                !!(self.Name && self.PrepTime && self.CookingTime)
                && (
                     !self.Ingredients().some(function (item) {
                         return !item.isValid()
                     })
                )
            )
        }

        self.deleteIngredient = function (ingredient) {
            self.Ingredients.remove(ingredient)
            if (!ingredient.isNew()) {
                self.deletedIngredients.push(ingredient)
            }
        }

        self.Ingredients = ko.observableArray([]);

        self.addIngredient = function (src) {
            var rtn = new talbot.Ingredient(src)
            self.Ingredients.push(rtn);
            return rtn;
        }

        self.clearIngredients = function () {
            self.deletedIngredients = [];
            self.Ingredients([]);
        }

        self.isNew = function () {
            return !self.Id
        }

        self.hasChanges = function () {
            var orig = self.orig()
            if (self.CookingTime !== orig.CookingTime) {
                return true;
            }
            if (self.PrepTime !== orig.PrepTime) {
                return true;
            }
            if (self.CookingTime !== orig.CookingTime) {
                return true;
            }

            return false
        }
    }
}())