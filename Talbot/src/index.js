﻿$(function () {
    var model = new window.talbot.AppViewModel();
    ko.applyBindings(model);
    model.getRecipes();
})