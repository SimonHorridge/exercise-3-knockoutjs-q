﻿/// <reference path="C:\Users\Simon\Documents\visual studio 2013\Projects\Talbot\Talbot\lib/knockout-3.3.0.js" />
/// <reference path="ingredient.js" />
/// <reference path="recipe.js" />


(function () {
    var recipeUrl = "http://api.10goto20.com/v1/recipe/SHor";
    function ingredientUrl(recipe) {
        return recipeUrl + "/" + recipe.Id + "/ingredient"
    }

    "use strict";
    window.talbot = window.talbot || {};
    window.talbot.AppViewModel = AppViewModel


    function AppViewModel() {

        //note non-injected refs to $. IRL, dependencies would be injected.

        var self = this;

        self.recipes = ko.observableArray([]);
        self.deletedRecipes = [];

        self.addIngredient = function (currentRecipe) {
            currentRecipe.addIngredient(new talbot.Ingredient({
                Quantity: "",
                Name: ""
            }))
        }

        self.savedRecipeCount = ko.observable();

        self.addRecipe = function () {
            var recipe = new talbot.Recipe();
            self.recipes.push(recipe);
            return recipe;
        }

        self.deleteRecipe = function (recipe) {
            if (!recipe.isNew()) {
                self.deletedRecipes.push(recipe);
            }
            self.recipes.remove(recipe)
        }

        self.getRecipes = function () {

            var d = Q.defer()

            getRecipes().then(function (results) {
                self.recipes([]);
                results.map(function (item) {
                    self.recipes.push(new talbot.Recipe(item))
                })
                self.deletedRecipes = [];
            })
            .then(function () {
                self.getIngredients().then(function () {
                    d.resolve()
                })
            })
            return d.promise
        }

        function setIngridients(recipe) {
            var d = Q.defer()
            getIngredientsForRecipe(recipe).then(function (ingredients) {
                recipe.clearIngredients();
                ingredients.map(function (ingredient) {
                    recipe.addIngredient(ingredient)
                })
                d.resolve()
            })
            return d.promise
        }

        self.getIngredients = function () {
            var d = Q.defer()
            var promises = []
            self.recipes().map(function (recipe) {
                promises.push(
                    setIngridients(recipe)
                )
            })
            Q.all(promises).then(function (results) {

                results.map(function (item) { })

                d.resolve()
            })
            return d.promise;
        }

        self.saveChangesAndReload = function () {
            var d = Q.defer()

            if (
                self.recipes().some(function (recipe) {
                    return !recipe.isValid()
            })
            ) {
                alert('One or more recipes is invalid\nPlease check and try again')
                return d.promise;
            }

            saveChanges().then(function () {
                var promises = [];
                self.recipes().map(function (recipe) {
                    recipe.Ingredients().map(function (ingredient) {
                        if (ingredient.isNew()) {
                            promises.push(
                               createIngredient(ingredient, recipe)
                            )
                        } else if (recipe.hasChanges()) {
                            promises.push(
                                updateIngredient(ingredient, recipe)
                            )
                        }
                    })
                })

                Q.all(promises).then(function () {
                    self.getRecipes().then(function () {
                        d.resolve()
                    })
                })
            })

            return d.promise
        }

        function saveChanges() {
            var d = Q.defer()
            var promises = [];

            self.recipes().map(function (recipe) {
                if (recipe.isNew()) {
                    promises.push(
                        createRecipe(recipe)
                    )
                } else if (recipe.hasChanges()) {
                    promises.push(
                        updateRecipe(recipe)
                    )
                }

                recipe.deletedIngredients.map(function (ingredient) {
                    deleteIngredient(ingredient, recipe)
                })

            })
            self.deletedRecipes.map(function (recipe) {
                promises.push(
                    deleteRecipe(recipe)
                )
            })

            Q.all(
                promises
            ).then(function () {
                self.deletedRecipes = []
                d.resolve()
            })

            return d.promise;
        }

    }

    function deleteRecipe(recipe) {
        var d = Q.defer()

        $.ajax(recipeUrl + "/" + recipe.Id, {
            type: "delete",
            success: function (results) {
                d.resolve(results)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }

        });

        return d.promise

    }
    function updateRecipe(recipe) {
        var d = Q.defer()

        $.ajax(recipeUrl + "/" + recipe.Id, {
            data: ko.toJSON(recipe),
            type: "put",
            contentType: "application/json",
            success: function (results) {
                d.resolve(results)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }

        });
        return d.promise
    }

    function createRecipe(recipe) {
        var d = Q.defer()

        $.ajax(recipeUrl, {
            data: ko.toJSON(recipe),
            type: "post",
            contentType: "application/json",
            success: function (updatedRecipe) {
                recipe.Id = updatedRecipe.Id;
                d.resolve(updatedRecipe)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }
        });
        return d.promise
    }

    function deleteIngredient(ingredient, recipe) {

        var d = Q.defer()

        $.ajax(ingredientUrl(recipe) + "/" + ingredient.Id, {
            type: "delete",
            success: function (results) {
                d.resolve(results)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }
        });

        return d.promise

    }

    function updateIngredient(ingredient, recipe) {
        var d = Q.defer()

        $.ajax(ingredientUrl(recipe) + "/" + ingredient.Id, {
            data: ko.toJSON(ingredient),
            type: "put",
            contentType: "application/json",
            success: function (results) {
                d.resolve(results)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }

        });
        return d.promise
    }

    function createIngredient(ingredient, recipe) {
        var d = Q.defer()

        $.ajax(ingredientUrl(recipe), {
            data: ko.toJSON(ingredient),
            type: "post",
            contentType: "application/json",
            success: function (results) {
                d.resolve(results)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }
        });
        return d.promise
    }

    function getRecipes() {
        var d = Q.defer()
        $.ajax(recipeUrl, {
            type: "get",
            success: function (results) {
                d.resolve(results)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }

        });
        return d.promise
    }

    function getIngredientsForRecipe(recipe) {
        var d = Q.defer()
        $.ajax(ingredientUrl(recipe), {
            type: "get",
            success: function (results) {
                d.resolve(results)
            },
            error: function (message) {
                alert(message);
                d.reject(message)
            }

        });
        return d.promise
    }
}())